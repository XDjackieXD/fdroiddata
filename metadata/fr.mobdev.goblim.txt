Categories:Internet
License:GPLv3
Web Site:http://www.gobl.im
Source Code:https://github.com/Schoumi/Goblim
Issue Tracker:https://github.com/Schoumi/Goblim/issues
FlattrID:4455953

Auto Name:Goblim
Summary:Share your photos
Description:
Let's you share your photos on the Lut.im server of your choice. Use your own
server to regain control of your privacy!

* Multiple configurable hosting URLs -- Use your own server!
* Copy the sharing URL to clipboard or share it to another application
* Keep track of your storage lifetime
* HTTP & HTTPS support
* Share history with thumbnails, share control.
* Encryption of images on the server

For additional information on the client and server part, visite the
[http://lut.im/ project page].
.

Repo Type:git
Repo:https://github.com/Schoumi/Goblim

Build:1.0,2
    commit=release_v1
    subdir=app
    gradle=yes

Auto Update Mode:None
Update Check Mode:Tags
Current Version:1.0
Current Version Code:2
