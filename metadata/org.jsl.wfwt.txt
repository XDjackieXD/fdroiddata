Categories:Connectivity
License:AGPLv3
Web Site:
Source Code:https://github.com/js-labs/WalkieTalkie
Issue Tracker:https://github.com/js-labs/WalkieTalkie/issues

Auto Name:Walkie Talkie
Summary:WiFi Walkie Talkie
Description:
This program transmits sound recorded from microphone to some other devices
running the same program on the same network segment, so works like Walkie
Talkie radio.

Devices find each other by Android NSD (aka Bonjour), no any configuration
required. Unfortunately Android NSD implementation is not stable enough, so
sometimes application can not establish connection properly. Application restart
or device reboot usually helps. Audio data is being transmitted by the unicast
channel, so each device works as a server and as a client at the same time.

Program was implemented as a demonstration of JS-Collider: Java high performance
scalable NIO framework, see [https://github.com/js-labs/js-collider].
.

Repo Type:git
Repo:https://github.com/js-labs/WalkieTalkie.git

Build:1.3,4
    commit=d4a40966e33de9919830d837ec7ecd2db66942e3
    gradle=yes

Build:1.4,5
    commit=29b6a6998c94e9715586d1e57fa951f029492909
    gradle=yes

Build:1.5,6
    disable=remove apk
    commit=627873982b2799e9c4ac6e3884b79fa54e235266
    gradle=yes

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.5
Current Version Code:6
