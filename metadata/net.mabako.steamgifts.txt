AntiFeatures:NonFreeNet
Categories:Internet
License:MIT
Author Name:Marcus Bauer
Author Email:mabako@gmail.com
Web Site:
Source Code:https://github.com/SteamGifts/SteamGifts
Issue Tracker:https://github.com/SteamGifts/SteamGifts/issues
Donate:https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=Y6WJZY2VJHC6G
Bitcoin:1NQBKppWPZiE5PshLxqfFW4pgsnAv9irEu

Name:sg for SteamGifts
Summary:SteamGifts client
Description:
Browse giveaways and discussions on SteamGifts.

* browse, search, enter and comment on giveaways
* filter giveaways by type, levels, points and entries
* view and search through Discussions
* manage your account
* SGTools.info integration
.

Repo Type:git
Repo:https://github.com/SteamGifts/SteamGifts

Build:1.1,1001500
    commit=v1.1
    subdir=app
    gradle=vanilla

Auto Update Mode:Version v%v
Update Check Mode:HTTP
Update Check Data:https://steamgifts.github.io/SteamGifts/metadata/fdroid-version.txt|versionCode=(.*)|.|versionName=(.*)
Current Version:1.1
Current Version Code:1001500
