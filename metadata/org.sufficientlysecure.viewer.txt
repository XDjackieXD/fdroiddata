Categories:Reading
License:GPLv3
Web Site:http://www.sufficientlysecure.org/android
Source Code:https://github.com/SufficientlySecure/document-viewer
Issue Tracker:https://github.com/SufficientlySecure/document-viewer/issues
Donate:http://www.sufficientlysecure.org/android

Auto Name:Document Viewer
Summary:Viewer for many document formats
Description:
Document Viewer supports:

* PDF
* DjVu
* XPS (OpenXPS)
* Comic Books (cbz) (NO support for cbr (rar compressed))
* FictionBook (fb2, fb2.zip)

This apk supports all ABIs: ARM, x86 and MIPS.
.

Repo Type:git
Repo:https://github.com/SufficientlySecure/document-viewer.git

Build:2.1,2100
    commit=v2.1
    subdir=document-viewer
    buildjni=yes

Build:2.2,2200
    commit=v2.2
    subdir=document-viewer
    buildjni=yes

Build:2.4,2400
    commit=6888f6bae47cf1a47d
    subdir=document-viewer
    buildjni=yes

Build:2.5,2500
    commit=v2.5
    subdir=document-viewer
    gradle=yes
    buildjni=yes

Build:2.6,2600
    commit=v2.6
    subdir=document-viewer
    submodules=yes
    gradle=yes
    prebuild=pushd ../ && \
        ./init.sh && \
        popd
    scandelete=document-viewer/jni/mupdf/mupdf/build/debug/
    buildjni=yes

Build:2.6.1,2610
    commit=v2.6.1
    subdir=document-viewer
    submodules=yes
    gradle=yes
    prebuild=pushd ../ && \
        ./init.sh && \
        popd
    scandelete=document-viewer/jni/mupdf/mupdf/build/debug/
    buildjni=yes

Build:2.7,2700
    commit=v2.7
    subdir=document-viewer
    submodules=yes
    gradle=yes
    prebuild=pushd ../ && \
        ./init.sh && \
        popd
    scandelete=document-viewer/jni/mupdf/mupdf/build/debug/
    buildjni=yes

Build:2.7.1,2710
    commit=v2.7.1
    subdir=document-viewer
    submodules=yes
    gradle=yes
    prebuild=pushd ../ && \
        ./init.sh && \
        popd
    scandelete=document-viewer/jni/mupdf/mupdf/build/debug/
    buildjni=yes

Build:2.7.2,2720
    commit=v2.7.2
    subdir=document-viewer
    submodules=yes
    gradle=yes
    prebuild=sed -i -e '/splits/,+7d' build.gradle && \
        pushd ../ && \
        ./init.sh && \
        popd
    scandelete=document-viewer/jni/mupdf/mupdf/build/debug/
    buildjni=yes

Build:2.7.3,2730
    disable=doesn't build
    commit=v2.7.3
    subdir=document-viewer
    submodules=yes
    gradle=yes
    prebuild=sed -i -e '/splits/,+7d' build.gradle && \
        pushd ../ && \
        ./init.sh && \
        popd
    scandelete=document-viewer/jni/mupdf/mupdf/build/debug/
    buildjni=yes

Build:2.7.3,2731
    commit=v2.7.3
    subdir=document-viewer
    submodules=yes
    gradle=yes
    forcevercode=yes
    prebuild=sed -i -e '/splits/,+7d' build.gradle && \
        sed -i -e '/applicationVariants\.all/,+5d' build.gradle && \
        sed -i -e '/ext\.versionCode/d' build.gradle && \
        echo "APP_ABI := armeabi" > jni/Application.mk && \
        pushd ../ && \
        ./init.sh && \
        popd
    scandelete=document-viewer/jni/mupdf/mupdf/build/debug/
    buildjni=yes

Build:2.7.3,2732
    commit=v2.7.3
    subdir=document-viewer
    submodules=yes
    gradle=yes
    forcevercode=yes
    prebuild=sed -i -e '/splits/,+7d' build.gradle && \
        sed -i -e '/applicationVariants\.all/,+5d' build.gradle && \
        sed -i -e '/ext\.versionCode/d' build.gradle && \
        echo "APP_ABI := armeabi-v7a" > jni/Application.mk && \
        pushd ../ && \
        ./init.sh && \
        popd
    scandelete=document-viewer/jni/mupdf/mupdf/build/debug/
    buildjni=yes

Build:2.7.3,2733
    commit=v2.7.3
    subdir=document-viewer
    submodules=yes
    gradle=yes
    forcevercode=yes
    prebuild=sed -i -e '/splits/,+7d' build.gradle && \
        sed -i -e '/applicationVariants\.all/,+5d' build.gradle && \
        sed -i -e '/ext\.versionCode/d' build.gradle && \
        echo "APP_ABI := x86" > jni/Application.mk && \
        pushd ../ && \
        ./init.sh && \
        popd
    scandelete=document-viewer/jni/mupdf/mupdf/build/debug/
    buildjni=yes

Build:2.7.3,2734
    commit=v2.7.3
    subdir=document-viewer
    submodules=yes
    gradle=yes
    forcevercode=yes
    prebuild=sed -i -e '/splits/,+7d' build.gradle && \
        sed -i -e '/applicationVariants\.all/,+5d' build.gradle && \
        sed -i -e '/ext\.versionCode/d' build.gradle && \
        echo "APP_ABI := mips" > jni/Application.mk && \
        pushd ../ && \
        ./init.sh && \
        popd
    scandelete=document-viewer/jni/mupdf/mupdf/build/debug/
    buildjni=yes

Build:2.7.3,2735
    commit=v2.7.3
    subdir=document-viewer
    submodules=yes
    gradle=yes
    forcevercode=yes
    prebuild=sed -i -e '/splits/,+7d' build.gradle && \
        sed -i -e '/applicationVariants\.all/,+5d' build.gradle && \
        sed -i -e '/ext\.versionCode/d' build.gradle && \
        echo "APP_ABI := arm64-v8a" > jni/Application.mk && \
        pushd ../ && \
        ./init.sh && \
        popd
    scandelete=document-viewer/jni/mupdf/mupdf/build/debug/
    buildjni=yes

Build:2.7.3,2736
    commit=v2.7.3
    subdir=document-viewer
    submodules=yes
    gradle=yes
    forcevercode=yes
    prebuild=sed -i -e '/splits/,+7d' build.gradle && \
        sed -i -e '/applicationVariants\.all/,+5d' build.gradle && \
        sed -i -e '/ext\.versionCode/d' build.gradle && \
        echo "APP_ABI := x86_64" > jni/Application.mk && \
        pushd ../ && \
        ./init.sh && \
        popd
    scandelete=document-viewer/jni/mupdf/mupdf/build/debug/
    buildjni=yes

Build:2.7.3,2737
    commit=v2.7.3
    subdir=document-viewer
    submodules=yes
    gradle=yes
    forcevercode=yes
    prebuild=sed -i -e '/splits/,+7d' build.gradle && \
        sed -i -e '/applicationVariants\.all/,+5d' build.gradle && \
        sed -i -e '/ext\.versionCode/d' build.gradle && \
        echo "APP_ABI := mips64" > jni/Application.mk && \
        pushd ../ && \
        ./init.sh && \
        popd
    scandelete=document-viewer/jni/mupdf/mupdf/build/debug/
    buildjni=yes

Maintainer Notes:
* Check init.sh from time to time.
* Archive policy last three upstream releases
* Disable AUM since it does not support ABI splits
.

Archive Policy:10 versions
Auto Update Mode:None
# Auto Update Mode:Version v%v
Update Check Mode:Tags
Vercode Operation:%c + 7
Current Version:2.7.3
Current Version Code:2737
