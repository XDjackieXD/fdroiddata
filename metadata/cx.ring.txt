Categories:Phone & SMS,Internet
License:GPLv3+
Web Site:https://ring.cx/
Source Code:
Issue Tracker:https://tuleap.ring.cx/projects/ring/

Name:Ring
Summary:Secure and distributed communication platform
Description:
Ring (formerly SFLphone) is a free distributed multimedia communication
software. It is developed by Savoir-faire Linux with the help of a global
community of users and contributors. Savoir-faire Linux is a Canadian company
specialized in Linux and free software.
.

Repo Type:git
Repo:https://gerrit-ring.savoirfairelinux.com/ring-client-android

Build:2.0.003,20
    commit=0011b9ba52b6ac3aab97a2bf42a608f83f0ec15c
    output=ring-android/app/build/outputs/apk/app-release-unsigned.apk
    build=echo -e '#!/bin/sh\ngradle "$@"' > ring-android/gradlew && \
        export ANDROID_NDK_ROOT=$ANDROID_NDK && \
        export ANDROID_ABI="armeabi-v7a" && \
        ./compile.sh release

Build:2.0.009,27
    commit=4423b10e1dd8601d00486e1b78d39180cb45f410
    subdir=ring-android/app/src/main
    output=../../build/outputs/apk/app-release-unsigned.apk
    build=pushd ../../../../ && \
        echo -e '#!/bin/sh\ngradle "$@"' > ring-android/gradlew && \
        export ANDROID_NDK_ROOT=$ANDROID_NDK && \
        export ANDROID_ABI="armeabi-v7a" && \
        ./compile.sh release && \
        popd

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:2.0.009
Current Version Code:27
