Categories:System
License:GPLv3
Web Site:
Source Code:https://github.com/WIStudent/FreifunkAutoConnectApp
Issue Tracker:https://github.com/WIStudent/FreifunkAutoConnectApp/issues
Changelog:https://github.com/WIStudent/FreifunkAutoConnectApp/blob/HEAD/CHANGELOG.md

Auto Name:Freifunk Auto Connect
Summary:Add multiple Freifunk SSIDs to your device
Description:
Compantion tool that makes it easier to add multiple Freifunk SSIDs to the
network configuration of your device.
.

Repo Type:git
Repo:https://github.com/WIStudent/FreifunkAutoConnectApp

Build:0.4,4
    commit=v0.4
    subdir=app
    gradle=yes

Build:0.5,5
    commit=v0.5
    subdir=app
    gradle=yes

Build:0.6,6
    disable=play-services
    commit=v0.6
    subdir=app
    gradle=yes

Auto Update Mode:None
Update Check Mode:Tags
Current Version:0.6.1
Current Version Code:7
