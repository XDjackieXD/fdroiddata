Categories:Multimedia,Reading
License:Apache2
Web Site:
Source Code:https://github.com/inorichi/tachiyomi
Issue Tracker:https://github.com/inorichi/tachiyomi/issues

Auto Name:Tachiyomi
Summary:Manga reader
Description:
Keep in mind it's still a beta, so expect it to crash sometimes.

Current features:

* Online and offline reading
* Configurable reader with multiple viewers and settings
* MyAnimeList support
* Resume from the next unread chapter
* Chapter filtering
* Schedule searching for updates
* Categories to organize your library
.

Repo Type:git
Repo:https://github.com/inorichi/tachiyomi.git

Build:0.1.2,3
    commit=v0.1.2
    subdir=app
    gradle=yes

Build:0.1.3,4
    commit=v0.1.3
    subdir=app
    gradle=yes

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:0.1.3
Current Version Code:4
