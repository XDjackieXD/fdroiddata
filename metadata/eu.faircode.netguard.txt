Categories:Security,Internet
License:GPLv3
Web Site:
Source Code:https://github.com/M66B/NetGuard
Issue Tracker:https://github.com/M66B/NetGuard/issues

Auto Name:NetGuard
Summary:Block network access
Description:
Simple way to block access to the internet - no root required. Applications can
individually be allowed or denied access via your Wi-Fi and/or mobile
connection.
.

Repo Type:git
Repo:https://github.com/M66B/NetGuard

Build:0.7,7
    commit=42ad6c2c8137cb82f73fa2efd5e5f47959f92f17
    subdir=app
    gradle=yes
    prebuild=sed -i -e '/applicationVariants/,+2d' -e '/def setOutputFile/i/*' -e '/dependencies {/i*/' build.gradle

Build:0.12,12
    commit=0.12
    subdir=app
    gradle=yes
    prebuild=sed -i -e '/applicationVariants/,+2d' -e '/def setOutputFile/i/*' -e '/dependencies {/i*/' build.gradle

Build:0.13,13
    commit=0.13
    subdir=app
    gradle=yes
    prebuild=sed -i -e '/applicationVariants/,+2d' -e '/def setOutputFile/i/*' -e '/dependencies {/i*/' build.gradle

Build:0.14,14
    commit=0.14
    subdir=app
    gradle=yes
    prebuild=sed -i -e '/applicationVariants/,+2d' -e '/def setOutputFile/i/*' -e '/dependencies {/i*/' build.gradle

Build:0.16,16
    commit=0.16
    subdir=app
    gradle=yes
    prebuild=sed -i -e '/applicationVariants/,+2d' -e '/def setOutputFile/i/*' -e '/dependencies {/i*/' build.gradle

Build:0.17,17
    commit=0.17
    subdir=app
    gradle=yes
    prebuild=sed -i -e '/applicationVariants/,+2d' -e '/def setOutputFile/i/*' -e '/dependencies {/i*/' build.gradle

Build:0.18,18
    commit=0.18
    subdir=app
    gradle=yes
    prebuild=sed -i -e '/applicationVariants/,+2d' -e '/def setOutputFile/i/*' -e '/dependencies {/i*/' build.gradle

Build:0.19,19
    commit=0.19
    subdir=app
    gradle=yes
    prebuild=sed -i -e '/applicationVariants/,+2d' -e '/def setOutputFile/i/*' -e '/dependencies {/i*/' build.gradle

Build:0.21,21
    commit=0.21
    subdir=app
    gradle=yes
    prebuild=sed -i -e '/applicationVariants/,+2d' -e '/def setOutputFile/i/*' -e '/dependencies {/i*/' build.gradle

Build:0.24,24
    commit=0.24
    subdir=app
    gradle=yes
    prebuild=sed -i -e '/applicationVariants/,+2d' -e '/def setOutputFile/i/*' -e '/dependencies {/i*/' build.gradle

Build:0.28,28
    commit=0.28
    subdir=app
    gradle=yes
    prebuild=sed -i -e '/applicationVariants/,+2d' -e '/def setOutputFile/i/*' -e '/dependencies {/i*/' build.gradle

Build:0.29,29
    commit=0.29
    subdir=app
    gradle=yes
    prebuild=sed -i -e '/applicationVariants/,+2d' -e '/def setOutputFile/i/*' -e '/dependencies {/i*/' build.gradle

Build:0.32,32
    disable=critical bug
    commit=0.32
    subdir=app
    gradle=yes
    prebuild=sed -i -e '/applicationVariants/,+2d' -e '/def setOutputFile/i/*' -e '/dependencies {/i*/' build.gradle

Build:0.33,33
    commit=0.33
    subdir=app
    gradle=yes
    prebuild=sed -i -e '/applicationVariants/,+2d' -e '/def setOutputFile/i/*' -e '/dependencies {/i*/' build.gradle

Build:0.34,34
    commit=0.34
    subdir=app
    gradle=yes
    prebuild=sed -i -e '/applicationVariants/,+2d' -e '/def setOutputFile/i/*' -e '/dependencies {/i*/' build.gradle

Build:0.35,35
    commit=0.35
    subdir=app
    gradle=yes
    prebuild=sed -i -e '/applicationVariants/,+2d' -e '/def setOutputFile/i/*' -e '/dependencies {/i*/' build.gradle

Build:0.37,37
    commit=0.37
    subdir=app
    gradle=yes
    prebuild=sed -i -e '/applicationVariants/,+2d' -e '/def setOutputFile/i/*' -e '/dependencies {/i*/' build.gradle

Build:0.62,2015122301
    commit=0.62
    subdir=app
    init=echo 'task wrapper(type: Wrapper) { gradleVersion = "2.8" }' >> ../build.gradle
    gradle=yes

Build:0.63,2015122501
    commit=0.63
    subdir=app
    init=echo 'task wrapper(type: Wrapper) { gradleVersion = "2.8" }' >> ../build.gradle
    gradle=yes

Build:0.64,2015122801
    commit=0.64
    subdir=app
    init=echo 'task wrapper(type: Wrapper) { gradleVersion = "2.8" }' >> ../build.gradle
    gradle=yes

Build:0.65,2016010301
    commit=0.65
    subdir=app
    init=echo 'task wrapper(type: Wrapper) { gradleVersion = "2.8" }' >> ../build.gradle
    gradle=yes

Build:0.66,2016010401
    commit=0.66
    subdir=app
    init=echo 'task wrapper(type: Wrapper) { gradleVersion = "2.8" }' >> ../build.gradle
    gradle=yes

Build:0.67,2016010501
    commit=0.67
    subdir=app
    init=echo 'task wrapper(type: Wrapper) { gradleVersion = "2.8" }' >> ../build.gradle
    gradle=yes

Build:0.68,2016010502
    commit=0.68
    subdir=app
    init=echo 'task wrapper(type: Wrapper) { gradleVersion = "2.8" }' >> ../build.gradle
    gradle=yes

Build:0.69,2016010701
    commit=0.69
    subdir=app
    init=echo 'task wrapper(type: Wrapper) { gradleVersion = "2.9" }' >> ../build.gradle
    gradle=yes

Build:0.72,2016010803
    commit=0.72
    subdir=app
    init=echo 'task wrapper(type: Wrapper) { gradleVersion = "2.9" }' >> ../build.gradle
    gradle=yes

Build:0.73,2016010901
    commit=0.73
    subdir=app
    init=echo 'task wrapper(type: Wrapper) { gradleVersion = "2.9" }' >> ../build.gradle
    gradle=yes

Build:0.75,2016011004
    commit=0.75
    subdir=app
    init=echo 'task wrapper(type: Wrapper) { gradleVersion = "2.9" }' >> ../build.gradle
    gradle=yes

Build:0.76,2016011801
    commit=0.76
    subdir=app
    init=echo 'task wrapper(type: Wrapper) { gradleVersion = "2.9" }' >> ../build.gradle
    gradle=yes

Build:0.77-beta,2016012301
    disable=wrong tag, beta
    commit=0.77-7
    subdir=app
    init=echo 'task wrapper(type: Wrapper) { gradleVersion = "2.9" }' >> ../build.gradle
    gradle=yes

Build:0.78,2016012701
    commit=0.78
    subdir=app
    init=echo 'task wrapper(type: Wrapper) { gradleVersion = "2.9" }' >> ../build.gradle
    gradle=yes

Build:0.79-beta,2016012801
    disable=beta
    commit=0.79
    subdir=app
    init=echo 'task wrapper(type: Wrapper) { gradleVersion = "2.9" }' >> ../build.gradle
    gradle=yes

Build:0.80-beta,2016013001
    disable=beta
    commit=0.80
    subdir=app
    init=echo 'task wrapper(type: Wrapper) { gradleVersion = "2.9" }' >> ../build.gradle
    gradle=yes

Build:0.82-beta,2016020301
    disable=beta, build tested
    commit=0.82
    subdir=app
    init=echo 'task wrapper(type: Wrapper) { gradleVersion = "2.9" }' >> ../build.gradle
    gradle=yes

Build:0.83,2016020401
    commit=0.83
    subdir=app
    init=echo 'task wrapper(type: Wrapper) { gradleVersion = "2.9" }' >> ../build.gradle
    gradle=yes

Auto Update Mode:None
Update Check Mode:Tags ^[0-9.]*$
Current Version:0.85-beta
Current Version Code:2016020601
