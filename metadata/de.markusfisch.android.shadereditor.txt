Categories:Development
License:MIT
Web Site:http://markusfisch.de
Source Code:https://github.com/markusfisch/ShaderEditor
Issue Tracker:https://github.com/markusfisch/ShaderEditor/issues

Auto Name:Shader Editor
Summary:Create and edit GLSL shaders
Description:
Create and edit GLSL shaders on your Android phone or tablet and use them as
live wallpaper.
.

Repo Type:git
Repo:https://github.com/markusfisch/ShaderEditor.git

Build:1.6.3,12
    commit=1.6.3

Build:2.0.1,14
    commit=2.0.1
    subdir=ShaderEditor
    gradle=yes

Build:2.0.3,16
    commit=2.0.3
    subdir=ShaderEditor
    gradle=yes

Build:2.1.0,17
    commit=2.1.0
    subdir=ShaderEditor
    gradle=yes

Build:2.2.0,18
    commit=2.2.0
    subdir=ShaderEditor
    gradle=yes

Build:2.3.0,19
    commit=2.3.0
    subdir=ShaderEditor
    gradle=yes

Auto Update Mode:Version %v
Update Check Mode:Tags
Current Version:2.3.0
Current Version Code:19
